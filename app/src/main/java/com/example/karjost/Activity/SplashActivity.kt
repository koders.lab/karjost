package com.example.karjost.Activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.example.karjost.ApiService
import com.example.karjost.R
import com.example.karjost.Util.PreferencesUtils
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {



    var  isLogin = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        imageView.setImageResource(R.drawable.karjost)

        var apiService = ApiService(this)
        apiService.getAppData()

        val preferenceUtils = PreferencesUtils(this)
        isLogin = preferenceUtils.isLogin()




        var intent: Intent
        if(isLogin){
            intent =  Intent(this, MainActivity::class.java)
        } else {
            intent = Intent(this, LoginActivity::class.java)
        }
         Handler().postDelayed(Runnable {

             startActivity(intent)
             finish()
         }, 15500)




    }
}
